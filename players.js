module.exports = [
  {
    id: "gryffindor",
    name: "Гриффиндор",
    picture: "https://vignette.wikia.nocookie.net/harrypotter/images/8/8e/0.31_Gryffindor_Crest_Transparent.png/revision/latest/scale-to-width-down/700?cb=20161124074004",
    repo: "git@bitbucket.org:GrakovNe/hogwarts-tournament-solution-gryffindor.git",
  },
  {
    id: "ravenclaw",
    name: "Когтевран",
    picture: "https://vignette.wikia.nocookie.net/harrypotter/images/b/b8/Ravenclaw1.png/revision/latest?cb=20150807113300&path-prefix=ru",
    repo: "git@bitbucket.org:art_kuznetsov/hogwarts-tournament-solution-sample.git",
  },
  {
    id: "slytherin",
    name: "Слизерин",
    picture: "https://vignette.wikia.nocookie.net/harrypotter/images/d/d3/0.61_Slytherin_Crest_Transparent.png/revision/latest/scale-to-width-down/700?cb=20161020182557",
    repo: "git@bitbucket.org:orlovpn/hogwarts-tournament-solution-slytherin.git",
  },
  {
    id: "hufflepuff",
    name: "Пуффендуй",
    picture: "https://vignette.wikia.nocookie.net/harrypotter/images/5/50/0.51_Hufflepuff_Crest_Transparent.png/revision/latest/scale-to-width-down/700?cb=20161020182518",
    repo: "git@bitbucket.org:vevsyukov/hogwarts-tournament-solution-hufflepuff.git",
  },
  {
    id: "dementors",
    name: "Дементоры",
    picture: "https://vignette.wikia.nocookie.net/harrypotter/images/b/ba/%D0%94%D0%B5%D0%BC%D0%B5%D0%BD%D1%82%D0%BE%D1%80_Pottermore.png/revision/latest?cb=20150521163933&path-prefix=ru",
    repo: "git@bitbucket.org:Brainus25/hogwarts-tournament-solution-dementors.git"
  }
];
