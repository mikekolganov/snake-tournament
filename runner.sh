#!/usr/bin/env bash
python snake.py $1 &
sleep 60
kill $! 2>/dev/null && echo "PROGRAM DIDN'T FINISHED IN 60s, KILLED"
