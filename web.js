const Koa = require("koa");
const serve = require('koa-static');
const Router = require("koa-router");
const Handlebars = require('handlebars');
const fs = require('fs');
const keys = require('lodash/keys');
const map = require('lodash/map');
const maxBy = require('lodash/maxBy');
const reverse = require('lodash/reverse');
const sortBy = require('lodash/sortBy');
const take = require('lodash/take');

const app = new Koa();
const router = new Router();

const layout = Handlebars.compile(fs.readFileSync('./views/layout.html').toString());
const all = Handlebars.compile(fs.readFileSync('./views/all.html').toString());
const player = Handlebars.compile(fs.readFileSync('./views/player.html').toString());
const recording = Handlebars.compile(fs.readFileSync('./views/recording.html').toString());

router.get('/', function (ctx, next) {
  const players = require('./players.js');
  let scope = [];
  players.forEach((player) => {
    const files = JSON.parse(fs.readFileSync(`./public/results/${player.id}.json`));
    const records = keys(files);
    const best_record = maxBy(records, (filename) => {
      return parseInt(files[filename].result_score, 10);
    });
    scope.push(Object.assign({}, player, {
      best_record: best_record,
      best_score: files[best_record].result_score
    }));
  });

  scope = reverse(sortBy(scope, (result) => parseInt(result.best_score, 10)));


  const body = all({ teams: scope});
  ctx.body = layout({ body: body });
});

router.get('/:player_id', function (ctx, next) {
  const files = JSON.parse(fs.readFileSync(`./public/results/${ctx.params.player_id}.json`));
  const records = take(reverse(map(files, (value, key) => {
    value.filename = key;
    return value;
  })), 30); 

  ctx.body = layout({ body: player({player_id: ctx.params.player_id, recordings: records}) });
});

router.get('/recordings/:recording_id', function (ctx, next) {
  const sp_param = parseInt(ctx.query.speed);
  const speed = 0.01 * (sp_param > 1 ? sp_param : 1);
  ctx.body = layout({ body: recording({speed: speed, file: ctx.params.recording_id}) });
});

app
  .use(serve('public'))
  .use(router.routes())
  .listen(3000);
