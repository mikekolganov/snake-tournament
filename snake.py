# SNAKES GAME
# Use ARROW KEYS to play, SPACE BAR for pausing/resuming and Esc Key for exiting

import curses
import sys
from curses import KEY_RIGHT, KEY_LEFT, KEY_UP, KEY_DOWN
from random import randint

import logging

logging.basicConfig(filename='public/results/{}.log'.format(sys.argv[1]), level=logging.DEBUG)
logger = logging.getLogger('snake_server')

client_module =  "solutions.{}.client".format(sys.argv[1] if len(sys.argv) > 1  else "sample")
step = getattr(__import__(client_module, fromlist=["step"]), "step")

FIELD_HEIGHT = 24
FIELD_WIDTH = 80
TIMEOUT = 1            # ms. Change to increase/decrease game the speed of snake

curses.initscr()
# Creates a new window of with given height, width, begin_y, begin_x
win = curses.newwin(FIELD_HEIGHT, FIELD_WIDTH, 0, 0)
win.keypad(1)
curses.noecho()
curses.curs_set(0)
win.border(0)
win.nodelay(1)

key = KEY_RIGHT  # Initializing values
score = 0

key_to_direction = {
    KEY_LEFT: 'left',
    KEY_RIGHT: 'right',
    KEY_UP: 'up',
    KEY_DOWN: 'down'
}

direction_to_key = dict(map(reversed, key_to_direction.items()))

snake = [[4, 10], [4, 9], [4, 8]]  # Initial snake co-ordinates
food = [randint(1, FIELD_HEIGHT - 2), randint(1, FIELD_WIDTH - 2)]  # First food co-ordinates

win.addch(food[0], food[1], '*')  # Prints the food

while key != 27:  # While Esc key is not pressed
    win.border(0)
    win.addstr(0, 2, 'Score : ' + str(score) + ' ')  # Printing 'Score' and
    win.addstr(0, 27, ' SNAKE ')  # 'SNAKE' strings
    win.timeout(TIMEOUT - (len(snake) / 5 + len(snake) / 10) % 120)  # Increases the speed of Snake as its length increases

    prev_key = key  # Previous key pressed
    event = win.getch()
    logger.debug('snake_coordinates: %s; food_coordinates: %s; direction: %s; field_size: %s' % (snake[:], food[:], key_to_direction[prev_key], { 'height': FIELD_HEIGHT, 'width': FIELD_WIDTH }))
    
    try:
        new_direction = step(snake_coordinates=snake[:],         # Pass copy of snake array to the step function
                         food_coordinates=food[:],           # Pass copy of food array o the step function
                         direction=key_to_direction[prev_key],
                         field_size={
                             'height': FIELD_HEIGHT,
                             'width': FIELD_WIDTH
                         })
    except Exception as e:
        logger.exception(e)
        break


    key = direction_to_key.get(new_direction) or key

    if key == ord(' '):  # If SPACE BAR is pressed, wait for another
        key = -1  # one (Pause/Resume)
        while key != ord(' '):
            key = win.getch()
        key = prev_key
        continue

    if key not in [KEY_LEFT, KEY_RIGHT, KEY_UP, KEY_DOWN, 27]:  # If an invalid key is pressed
        key = prev_key

    # Calculates the new coordinates of the head of the snake. NOTE: len(snake) increases.
    # This is taken care of later at [1].
    snake.insert(0, [snake[0][0] + (key == KEY_DOWN and 1) + (key == KEY_UP and -1),
                     snake[0][1] + (key == KEY_LEFT and -1) + (key == KEY_RIGHT and 1)])

    # If snake crosses the boundaries, make it enter from the other side (Uncomment to enable)
    # if snake[0][0] == 0: snake[0][0] = 18
    # if snake[0][1] == 0: snake[0][1] = 58
    # if snake[0][0] == 19: snake[0][0] = 1
    # if snake[0][1] == 59: snake[0][1] = 1

    # Exit if snake crosses the boundaries
    if snake[0][0] == 0 or snake[0][0] == FIELD_HEIGHT - 1 or snake[0][1] == 0 or snake[0][1] == FIELD_WIDTH - 1: break

    # If snake runs over itself
    if snake[0] in snake[1:]: break

    if snake[0] == food:  # When snake eats the food
        food = []
        score += 1
        while food == []:
            food = [randint(1, FIELD_HEIGHT - 2), randint(1, FIELD_WIDTH - 2)]  # Calculating next food's coordinates
            if food in snake: food = []
        win.addch(food[0], food[1], '*')
    else:
        last = snake.pop()  # [1] If it does not eat the food, length decreases
        win.addch(last[0], last[1], ' ')
    win.addch(snake[0][0], snake[0][1], '#')

curses.endwin()
logger.debug("\n\nSCORE: {}".format(str(score)));
print("\nScore - " + str(score))
