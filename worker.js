const players = require("./players.js");
const fs = require("fs");
const child_process = require("child_process");
const trim = require('lodash/trim');

function run() {
  const time = new Date().toISOString();

  console.log(`====== RUN AT ${time} ========`);

  players.forEach((player) => {
    if (! player.repo) return;
    console.log(`\n\n${player.id}`);
    if (! fs.existsSync(`${__dirname}/solutions/${player.id}`)) {
      console.log(child_process.execSync(`git clone ${player.repo} solutions/${player.id}`).toString());
    }
    else {
      console.log(child_process.execSync(`cd solutions/${player.id} && git pull`).toString());
    }

    const commitId = trim(child_process.execSync(`cd solutions/${player.id} && git rev-parse HEAD`).toString());

    const recordingFilename = `${player.id}_${commitId}_${time}.json`;

    child_process.execSync(`rm -f ./public/results/${player.id}.log`);
    console.log("RECORDING");
    const gameOutput = child_process.execSync(`asciinema rec -q -c "bash ./runner.sh ${player.id}" -t "${player.name} ${time}" ./public/results/${recordingFilename}`).toString();
    console.log("FINISH RECORDING");
    let score = 0;
    const match = gameOutput.match(/Score - (\d+)/);
    if (match && match[1]) {
      score = match[1];
    }

    let resultSummary = {};
    if (fs.existsSync(`${__dirname}/public/results/${player.id}.json`)) {
      resultSummary = require(`${__dirname}/public/results/${player.id}.json`);
    }

    resultSummary[recordingFilename] = {}
    resultSummary[recordingFilename].player_id = player.id;
    resultSummary[recordingFilename].player_name = player.name;
    resultSummary[recordingFilename].commit_id = commitId;
    resultSummary[recordingFilename].result_score = score;

    fs.writeFileSync(`${__dirname}/public/results/${player.id}.json`, JSON.stringify(resultSummary));

    console.log("SCORE ", score);
  });

  console.log(`\n====== FINISH AT ${time} ========`);
}
run();
setInterval(run, 1);
